#-------------------------------------------------
#
# Project created by QtCreator 2015-11-11T23:16:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = navigator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mycar.cpp

HEADERS  += mainwindow.h \
    mycar.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    start_point.png \
    point.png \
    finish_point.png \
    background.png

RESOURCES += \
    res.qrc
