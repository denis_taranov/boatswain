#ifndef MYCAR_H
#define MYCAR_H

class myCar
{
public:
    enum {NORTH, WEST, SOUTH, EAST};
    myCar(int cur_x = 0, int cur_y = 0, int direction = EAST);
    myCar(const myCar &a);
    void setCarPosition(int x, int y);
    void rotateCar(int left);
    int cur_x, cur_y;
    int dir;
private:
};

#endif // MYCAR_H
