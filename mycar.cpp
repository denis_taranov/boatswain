#include "mycar.h"

myCar::myCar(int x, int y, int direction)
{
    cur_x = x;
    cur_y = y;
    dir = direction;
}

myCar::myCar(const myCar &a)
{
    cur_x = a.cur_x;
    cur_y = a.cur_y;
    dir = a.dir;
}

void myCar::setCarPosition(int x, int y)
{
    cur_x = x;
    cur_y = y;
}

void myCar::rotateCar(int left)
{
    dir = (dir + left*2 + 1) % 4;
}
