#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    sx = sy = fx = fy = -1;
    isStarted = false;
    ui->setupUi(this);
    table = new QStandardItemModel(39, 39, this);
    for(int row = 0; row != table->rowCount(); ++row){
        for(int column = 0; column != table->columnCount(); ++column) {
            QStandardItem *newItem = new QStandardItem();
            table->setItem(row, column, newItem);
            if(row == 0 || row == table->rowCount() - 1 || column == 0 || column == table->columnCount() - 1){
                table->item(row, column)->setIcon(QIcon(":/images/point.png"));
                matrix[row][column] = -1;
            }

            else {
                table->item(row, column)->setIcon(QIcon(":/images/background.png"));
                matrix[row][column] = 0;
            }
        }
    }
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView->setSelectionMode(QAbstractItemView::NoSelection);
    ui->tableView->installEventFilter(this);
    ui->tableView->setModel(table);
    ui->dial->setEnabled(false);
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(100);
    QTime midnight(0, 0, 0);
    qsrand(midnight.secsTo(QTime::currentTime()));
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
        if (keyEvent->key() == Qt::Key_Up) {
            table->item(car.cur_x, car.cur_y)->setIcon(QIcon(":/images/background.png"));
            if(car.cur_x + d_x[car.dir] == fx && car.cur_y + d_y[car.dir] == fy) {ui->label_5->setText("Боцман, вы добрались до конечной точки! Поздравляем Вас");}
            if(matrix[car.cur_x + d_x[car.dir]][car.cur_y + d_y[car.dir]] != -1) {
                car.cur_x += d_x[car.dir];
                car.cur_y += d_y[car.dir];
            }

            for(int i = 0; i < way.size(); ++i)
                qDebug() << way[i].first << " " << way[i].second << "\n";
            qDebug() << "/////////\n";
            return true;
        }
        else if (keyEvent->key() == Qt::Key_Down) {
            return true;
        }
        else if (keyEvent->key() == Qt::Key_Left) {
            car.rotateCar(0);
            return true;
        }
        else if (keyEvent->key() == Qt::Key_Right) {
            car.rotateCar(1);
            return true;
        }
    }
    return QMainWindow::eventFilter(obj, event);
}

void MainWindow::update()
{
    if(isStarted) {
        if(way.size() > 2) {
            QVector<QPair <int, int> >::iterator tmp = std::find(way.begin(), way.end(), QPair<int, int>(car.cur_x, car.cur_y));
            if(tmp != way.end()) {
                for(QVector<QPair <int, int> >::iterator i = way.begin(); i != tmp; ++i)
                    way.erase(i);
                // way.erase(tmp);
                QString label = "Боцман, продолжайте движение на ";
                if(way[1].first - car.cur_x == 0 && way[1].second - car.cur_y == 1) label += "запад";
                else if(way[1].first - car.cur_x  == 0 && way[1].second - car.cur_y == -1) label += "восток";
                else if(way[1].first - car.cur_x  == 1 && way[1].second - car.cur_y == 0) label += "юг";
                else if(way[1].first - car.cur_x  == -1 && way[1].second - car.cur_y == 0) label += "север";
                ui->label_5->setText(label);
            }
            else {
                ui->label_5->setText("Вы сбились с пути");
            }
        }
        for(int row = 0; row < table->rowCount(); ++row)
            for(int col = 0; col < table->columnCount(); ++col)
                if(matrix[row][col] != -1)
                    table->item(row, col)->setIcon(QIcon(":/images/background.png"));
        table->item(sx, sy)->setIcon(QIcon(":/images/start_point.png"));
        table->item(fx, fy)->setIcon(QIcon(":/images/finish_point.png"));
        for(int i = 0; i < way.size(); i++) {
            table->item(way[i].first, way[i].second)->setIcon(QIcon(":/images/way_point.png"));
        }
        if(car.dir == myCar::EAST) {
            table->item(car.cur_x, car.cur_y)->setIcon(QIcon(":/images/EAST"));
            ui->dial->setValue(75);
        }
        else if(car.dir == myCar::WEST) {
            table->item(car.cur_x, car.cur_y)->setIcon(QIcon(":/images/WEST"));
            ui->dial->setValue(25);
        }
        else if(car.dir == myCar::SOUTH) {
            table->item(car.cur_x, car.cur_y)->setIcon(QIcon(":/images/SOUTH"));
            ui->dial->setValue(0);
        }
        else if(car.dir == myCar::NORTH) {
            table->item(car.cur_x, car.cur_y)->setIcon(QIcon(":/images/NORTH"));
            ui->dial->setValue(50);
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

QVector <QPair <int, int> > MainWindow::findWay(QPair<int, int> from, QPair<int, int> to)
{
    QVector<QPair <int, int> > que, ans(0);
    QVector <int> pred;
    QPair <int, int> cur, next;
    int tail = 1, head = 0;
    que.push_back(from);
    pred.push_back(-1);
    matrix[from.first][from.second] = 1;
    while(tail > head){
        cur = que[head];
        for(int i = 0; i < 4; ++i) {
            next.first = cur.first + d_x[i];
            next.second = cur.second + d_y[i];
            if(matrix[next.first][next.second] == 0) {
                matrix[next.first][next.second] = matrix[cur.first][cur.second] + 1;
                que.push_back(next);
                pred.push_back(head);
                tail++;
                //qDebug() << que[tail-1].first << " " << que[tail-1].second << "\n";
            }
            if(next.first == to.first && next.second == to.second){
                pred.push_back(head);
                break;
            }
        }
        if(next.first == to.first && next.second == to.second) break;
        head ++;
    }
    if(next.first == to.first && next.second == to.second) {
        ans.push_back(to);
        int cur = pred[tail];
        while(cur != -1) {
            ans.push_back(que[cur]);
            cur = pred[cur];
        }
    }
    std::reverse(ans.begin(), ans.end());
    return ans;
}

void MainWindow::start()
{
    car.setCarPosition(sx, sy);
    /////
    int tmp = 300 + qrand()%300;
    for(int i = 0; i < tmp; ++i) {
        int x = qrand()%39;
        int y = qrand()%39;
        if(!(sx == x && sy == y) && !(fx == x && fy == y)) {
            table->item(x, y)->setIcon(QIcon(":/images/point.png"));
            matrix[x][y] = -1;
        }
    }
    /////
    way = findWay(QPair<int, int>(sx, sy), QPair<int, int>(fx, fy));
    for(int i = 0; i < way.size(); i++) {
        table->item(way[i].first, way[i].second)->setIcon(QIcon(":/images/way_point.png"));
    }
}

void MainWindow::on_tableView_clicked(const QModelIndex &index)
{
    if(index.row() == 0 || index.row() == table->rowCount() - 1 ||
    index.column() == 0 || index.column() == table->columnCount() - 1) return;
    if(ui->radioButton->isChecked()) {
        table->item(index.row(), index.column())->setIcon(QIcon(":/images/start_point.png"));
        ui->radioButton->setCheckable(false);
        sx = index.row();
        sy = index.column();
    }
    else if(ui->radioButton_2->isChecked()) {
        table->item(index.row(), index.column())->setIcon(QIcon(":/images/finish_point.png"));
        ui->radioButton_2->setCheckable(false);
        fx = index.row();
        fy = index.column();
    }
    else if(ui->radioButton_3->isChecked()) {
        if(index.row() == sx && index.column() == sy) {
            sx = -1; sy = -1;
            ui->radioButton->setCheckable(true);
        }
        else if(index.row() == fx && index.column() == fy) {
            fx = -1; fy = -1;
            ui->radioButton_2->setCheckable(true);
        }
        table->item(index.row(), index.column())->setIcon(QIcon(":/images/point.png"));
        matrix[index.row()][index.column()] = -1;
    }
    else if(ui->radioButton_4->isChecked()) {
        if(index.row() == sx && index.column() == sy) {
            sx = -1; sy = -1;
            ui->radioButton->setCheckable(true);
        }
        else if(index.row() == fx && index.column() == fy) {
            fx = -1; fy = -1;
            ui->radioButton_2->setCheckable(true);
        }
        table->item(index.row(), index.column())->setIcon(QIcon(":/images/background.png"));
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    if(sx != -1 && fx != -1) {
        ui->radioButton->setCheckable(false);
        ui->radioButton_2->setCheckable(false);
        ui->radioButton_3->setCheckable(false);
        ui->radioButton_4->setCheckable(false);

        ui->radioButton->setEnabled(false);
        ui->radioButton_2->setEnabled(false);
        ui->radioButton_3->setEnabled(false);
        ui->radioButton_4->setEnabled(false);
        isStarted = true;
        start();
    }
    else {
        QMessageBox msgBox;
        msgBox.setText("Необходимо задать стартовую и конечную точки");
        msgBox.exec();
    }
}

void MainWindow::on_pushButton_clicked()
{
    sx = sy = fx = fy = -1;
    isStarted = false;
    for(int row = 0; row != table->rowCount(); ++row){
        for(int column = 0; column != table->columnCount(); ++column) {
            if(row == 0 || row == table->rowCount() - 1 || column == 0 || column == table->columnCount() - 1){
                table->item(row, column)->setIcon(QIcon(":/images/point.png"));
                matrix[row][column] = -1;
            }
            else {
                table->item(row, column)->setIcon(QIcon(":/images/background.png"));
                matrix[row][column] = 0;
            }
        }
    }
    ui->radioButton->setCheckable(true);
    ui->radioButton_2->setCheckable(true);
    ui->radioButton_3->setCheckable(true);
    ui->radioButton_4->setCheckable(true);

    ui->radioButton->setEnabled(true);
    ui->radioButton_2->setEnabled(true);
    ui->radioButton_3->setEnabled(true);
    ui->radioButton_4->setEnabled(true);
}
