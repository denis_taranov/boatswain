#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItemModel>
#include <QAbstractItemView>
#include <QDebug>
#include <QVector>
#include <QPair>
#include <QKeyEvent>
#include <QTimer>
#include "mycar.h"
#include <QTime>
#include <QMessageBox>

const int d_x[4] = {-1, 0, 1, 0}, d_y[4] = {0, -1, 0, 1};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void start();
    int sx, sy, fx, fy;
    int matrix[40][40];
    bool isStarted;
    QVector <QPair <int, int> > way;
    QVector <QPair <int, int> > findWay(QPair<int, int> from, QPair<int, int> to);
    ~MainWindow();

private slots:
    void on_tableView_clicked(const QModelIndex &index);

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void update();

private:
    Ui::MainWindow *ui;
    QStandardItemModel *table;
    myCar car;

protected:
    bool eventFilter(QObject *obj, QEvent *event);
};

#endif // MAINWINDOW_H
